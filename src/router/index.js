import Vue from 'vue'
import VueRouter from 'vue-router'
import Signin from '../views/Signin.vue'
import Signup from '../views/Signup.vue'
import ForgotPassword from '../views/ForgotPassword.vue'
import ResetPassword from '../views/ResetPassword.vue'
import Dashboard from '../views/Dashboard.vue'
import PilihLevel from '../views/PilihLevel.vue'
import TutorialHuruf from '../views/TutorialHuruf.vue'
import QuizHuruf from '../views/QuizHuruf.vue'
import Profile from '../views/Profile.vue'
import TutorialKata from '../views/TutorialKata.vue'
import QuizKata from '../views/QuizKata.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Signin',
    component: Signin
  },
  {
    path: '/Signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/Forgot-password',
    name: 'Forgot Password',
    component: ForgotPassword
  },
  {
    path: '/Reset-password',
    name: 'Reset Password',
    component: ResetPassword
  },
  {
    path: '/Dashboard',
    name: 'Dashboard',
    component : Dashboard
  },
  {
    path: '/Pilih-level',
    name: 'Pilih Level',
    component : PilihLevel

  },
  {
    path: '/Tutorial-huruf',
    name: 'Tutorial Huruf',
    component : TutorialHuruf

  },
  {
    path: '/Quiz-huruf',
    name: 'quiz Huruf',
    component : QuizHuruf

  },
  {
    path: '/Profile',
    name: 'Profile',
    component : Profile
  },
  {
    path: '/Tutorial-kata',
    name: 'Tutorial kata',
    component : TutorialKata
  },
  {
    path: '/Quiz-kata',
    name: 'Quiz kata',
    component : QuizKata
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
